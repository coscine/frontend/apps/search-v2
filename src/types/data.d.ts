export declare type DataType = {
  header: Array<DataEntry>;
  body: Array<DataEntry>;
};
export declare type DataEntry = {
  type: string;
  value: string;
};

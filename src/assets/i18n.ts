const locales = {
  i18n: {
    search: {
      en: {
        headline: "Search",
        archived: "Archived",

        search: "Search",

        btnSearchItem1: "Item 1",
        btnSearchItem2: "Item 2",

        emptySearch: "No item is found for the given search criteria",
        endSearchResults: "Showing all results",

        allProjects: "All Projects",
        allResources: "All Resources",
      },
      de: {
        headline: "Suche",
        archived: "Archiviert",

        search: "Suchen",

        btnSearchItem1: "Eintrag 1",
        btnSearchItem2: "Eintrag 2",

        emptySearch: "Es wurden keine Treffer gefunden",
        endSearchResults: "Alle Ergebnisse werden angezeigt",

        allProjects: "Alle Projekte",
        allResources: "Alle Resourcen",
      },
    },
  },
};

export default locales;

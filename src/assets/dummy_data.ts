import { DataEntry, DataType } from "@/types/data";

export const mainViewData: Array<DataType> = [
  {
    header: [
      {
        type: "Project",
        value: "SomeExampleProjectName1",
      },
    ],
    body: [
      {
        type: "Created By",
        value: "Max Mustermann",
      },
      {
        type: "Parent Project",
        value: "None",
      },
      {
        type: "Start Date",
        value: "01.01.2020",
      },
      {
        type: "End Date",
        value: "01.12.2022",
      },
      {
        type: "PI",
        value: "Prof. Müller",
      },
      {
        type: "Grant Id",
        value: "DFG 1866",
      },
    ],
  },
  {
    header: [
      {
        type: "Project",
        value: "SomeExampleProjectName2",
      },
    ],
    body: [
      {
        type: "Created By",
        value: "Manuel Rosenberger",
      },
      {
        type: "Parent Project",
        value: "None",
      },
      {
        type: "Start Date",
        value: "17.07.2020",
      },
      {
        type: "End Date",
        value: "-",
      },
      {
        type: "PI",
        value: "Prof. Müller",
      },
      {
        type: "Grant Id",
        value: "DFG 7753",
      },
    ],
  },
  {
    header: [
      {
        type: "Project",
        value: "SomeExampleProjectName3",
      },
    ],
    body: [
      {
        type: "Created By",
        value: "Hildegard Beck",
      },
      {
        type: "Parent Project",
        value: "SomeExampleProjectName1",
      },
      {
        type: "Start Date",
        value: "01.01.2020",
      },
      {
        type: "End Date",
        value: "01.12.2022",
      },
      {
        type: "PI",
        value: "Prof. Müller",
      },
      {
        type: "Grant Id",
        value: "DFG 1867",
      },
    ],
  },
  {
    header: [
      {
        type: "Resource",
        value: "SomeExampleResourceName",
        archived: true,
      } as DataEntry,
    ],
    body: [
      {
        type: "Persistent ID",
        value: "21.11102/0372a8a6-3f4f-448e-aa75-acb75-acb148fe66d6",
      },
      {
        type: "License",
        value: "MIT",
      },
      {
        type: "Usage Rights",
        value: "None",
      },
      {
        type: "Application Profile",
        value: "Radar",
      },
    ],
  },
  {
    header: [
      {
        type: "Resource",
        value: "Test-Resource",
      },
    ],
    body: [
      {
        type: "Persistent ID",
        value: "21.11102/cf39b3e6-e0b7-4415-bc65-acb75-fbbbc35b4b89",
      },
      {
        type: "License",
        value: "MIT",
      },
      {
        type: "Usage Rights",
        value: "None",
      },
      {
        type: "Application Profile",
        value: "Engmeta",
      },
    ],
  },
  {
    header: [
      {
        type: "Resource",
        value: "Epic Project Resource",
        archived: true,
      } as DataEntry,
    ],
    body: [
      {
        type: "Persistent ID",
        value: "21.11102/2a108fee-d60a-40e8-809a-acb75-8cf5aecb93f9",
      },
      {
        type: "License",
        value: "MIT",
      },
      {
        type: "Usage Rights",
        value: "None",
      },
      {
        type: "Application Profile",
        value: "Radar",
      },
    ],
  },
  {
    header: [
      {
        type: "Resource",
        value: "Epic Project Resource 2",
      },
    ],
    body: [
      {
        type: "Persistent ID",
        value: "21.11102/3f1bb053-644b-42c0-8e45-acb75-40193cde1a14",
      },
      {
        type: "License",
        value: "MIT",
      },
      {
        type: "Usage Rights",
        value: "None",
      },
      {
        type: "Application Profile",
        value: "Radar",
      },
    ],
  },
  {
    header: [
      {
        type: "File",
        value: "LabNotes.txt",
      },
    ],
    body: [
      {
        type: "MD1",
        value: "xxxxxxx",
      },
      {
        type: "MD2",
        value: "yyyyyyyy",
      },
      {
        type: "MD3",
        value: "None",
      },
      {
        type: "MD4",
        value: "wwwwwww",
      },
      {
        type: "MD5",
        value: "yyyyyyyyyyy",
      },
      {
        type: "MD6",
        value: "None",
      },
      {
        type: "MD7",
        value: "wwwwww",
      },
      {
        type: "MD8",
        value: "yyyyyyyyyyyyy",
      },
      {
        type: "MD9",
        value: "None",
      },
      {
        type: "MD10",
        value: "wwwwwwww",
      },
      {
        type: "MD11",
        value: "yyyyyyyyyyy",
      },
      {
        type: "MD12",
        value: "None",
      },
      {
        type: "MD13",
        value: "wwwwww",
      },
      {
        type: "MD14",
        value: "yyyyyyyyy",
      },
      {
        type: "MD15",
        value: "None",
      },
      {
        type: "MD16",
        value: "wwwwwwww",
      },
    ],
  },
  {
    header: [
      {
        type: "User",
        value: "Max Mustermann",
      },
    ],
    body: [
      {
        type: "Project Member",
        value:
          "SomeExampleProjectName1, SomeExampleProjectName2, SomeExampleProjectName3",
      },
    ],
  },
  {
    header: [
      {
        type: "User",
        value: "Manuel Rosenberger",
      },
    ],
    body: [
      {
        type: "Project Member",
        value: "SomeExampleProjectName2",
      },
    ],
  },
  {
    header: [
      {
        type: "User",
        value: "Hildegard Beck",
      },
    ],
    body: [
      {
        type: "Project Member",
        value: "SomeExampleProjectName1, SomeExampleProjectName3",
      },
    ],
  },
];

import { BootstrapVue } from "bootstrap-vue";
import Vue from "vue";
import SearchApp from "./SearchApp.vue";
import VueI18n from "vue-i18n";
import locales from "@/assets/i18n";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(BootstrapVue);

Vue.config.productionTip = false;
Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: "en",
  messages: locales.i18n.search,
  silentFallbackWarn: true,
});

new Vue({
  render: (h) => h(SearchApp),
  i18n,
}).$mount("search");
